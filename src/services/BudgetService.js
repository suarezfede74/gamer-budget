import {getHardware, getAMD, getIntel, getRX, getNvidia} from './HardwareService.js';

const getCheaperPCwithGPU = (cpus, gpus, rams, mothers) =>{
    let aux_pc = {
        cpu: '',
        gpu: '',
        ram: '',
        mother: '',
        price: 0
    };

    let aux = 999999;

    cpus.map((cpu) => {
        if (cpu.precio < aux){
            aux = cpu.precio
            aux_pc.cpu = cpu
        }
        return 0;
    })
    aux_pc.price += aux;

    aux = 999999;
    gpus.map((gpu) => {
        if (gpu.precio < aux){
            aux = gpu.precio
            aux_pc.gpu = gpu
        }
        return 0;
    })
    aux_pc.price += aux;

    aux = 999999;
    mothers.map((mother) => {
        if (mother.precio < aux && mother.socket === aux_pc.cpu.socket){
            aux = mother.precio
            aux_pc.mother = mother
        }
        return 0;
    })
    aux_pc.price += aux;
    aux = 999999;
    rams.map((ram) => {
        if (ram.precio < aux){
            aux = ram.precio
            aux_pc.ram = ram;
        }
        return 0;
    })
    aux_pc.price += aux;

    return aux_pc;
}


const getFinalBudget = (budget, hasDedicatedGpu, kind, cpus, gpus, rams, mothers) => {
    let aux_pc = {
        cpu: '',
        gpu: '',
        ram: '',
        mother: '',
        price: 0
    };
    let motherMaxPrice = budget * 0.22;
    let ramMaxPrice = budget * 0.12;
    let cpuMaxPrice;
    let gpuMaxPrice;
    if(!hasDedicatedGpu){
        cpuMaxPrice = budget * 0.56;
        ramMaxPrice = budget * 0.22;
    }
    else{
        if(kind === 'Gaming' || budget < 25000){
            cpuMaxPrice = budget * 0.25;
            gpuMaxPrice = budget * 0.46
        }
        else{
            if(budget < 35000){
                cpuMaxPrice = budget * 0.36;
                gpuMaxPrice = budget * 0.35 
            }
            else{
                ramMaxPrice = budget * 0.18;
                cpuMaxPrice = budget * 0.40;
                gpuMaxPrice = budget * 0.25 
            }
            
        }
    }

    if(motherMaxPrice > 8000){
        ramMaxPrice = ramMaxPrice + (motherMaxPrice - 6500);
        motherMaxPrice = 6500;
    }

    let aux = 0;
    if(kind === 'Gaming'){
        cpus.map((cpu) =>{
            if(cpu.precio <= cpuMaxPrice && cpu.mono > aux){
                aux_pc.cpu = cpu;
                aux = cpu.mono;
            }
            return 0;
        })
    }
    else{
        cpus.map((cpu) =>{
            if(cpu.precio <= cpuMaxPrice && cpu.multi > aux){
                aux_pc.cpu = cpu;
                aux = cpu.multi;
            }
            return 0;
        })
    }

    aux = 0;
    if(hasDedicatedGpu){
        gpus.map((gpu) =>{
            if(gpu.precio <= gpuMaxPrice && gpu.precio > aux){
                aux_pc.gpu = gpu;
                aux = gpu.precio;
            }
            return 0;
        })
    }

    aux = 0;
    mothers.map((mother) =>{
        if(mother.precio <= motherMaxPrice && mother.precio > aux && mother.socket === aux_pc.cpu.socket){
            aux_pc.mother = mother;
            aux = mother.precio;
        }
        return 0;
    })


    aux = 0;
    rams.map((ram) =>{
        if(ram.precio <= ramMaxPrice && ram.precio > aux){
            aux_pc.ram = ram;
            aux = ram.precio;
        }
        return 0;
    })

    aux_pc.price = aux_pc.cpu.precio + aux_pc.mother.precio + aux_pc.ram.precio + (aux_pc.gpu !== '' ? aux_pc.gpu.precio : 0)


    return aux_pc;
}

const getBudget = async (_budget, _kind, settings, setLoading) =>{
    let budgetFraction = _budget * 0.7;
    let finalBudget = {
        cpu: '',
        gpu: '',
        ram: '',
        mother: '',
        price: 0
    };

    const pref = {
        budget: _budget,
        gaming: _kind === 'Gaming',
        gpu: settings.pref.gpu === '' ? 'ANY' : settings.pref.gpu,
        cpu: settings.pref.cpu === '' ? 'ANY' : settings.pref.cpu
    };


    let res;
    switch(pref.cpu){
        case 'AMD': res = await getAMD(); break;
        case 'Intel': res = await getIntel(); break;
        default: res = await getHardware('procesadores'); break;
    }
    let procesadores = [];
    if(res.ok){
        res.data.map((cpu) => {
            let aux = {
                id: cpu[0],
                serial: cpu[1],
                name: cpu[2],
                precio: cpu[3],
                stock: cpu[4],
                mono: cpu[5],
                multi: cpu[6],
                consumo: cpu[7],
                socket: cpu[8],
            }
            procesadores.push(aux)
            return 0;
        })
    }


    switch(pref.gpu){
        case 'RX': res = await getRX(); break;
        case 'NVIDIA': res = await getNvidia(); break;
        default: res = await getHardware('gpu'); break;
    }
    let gpus = [];
    if(res.ok){
        res.data.map((gpu) => {
            let aux = {
                id: gpu[0],
                name: gpu[1],
                precio: gpu[2],
                stock: gpu[3],
                bench: gpu[5],
                consumo: gpu[6],
            }
            gpus.push(aux)
            return 0;
        })
    }


    res = await getHardware('ram')
    let rams = [];
    if(res.ok){
        res.data.map((ram) => {
            if(ram[4] !== 0){
                let aux = {
                    id: ram[0],
                    name: ram[1],
                    precio: ram[2],
                    stock: ram[3],
                    ddr: ram[4],
                    gb: ram[5],
                    mhz: ram[6]
                }
                rams.push(aux)
            }
            return 0;
        })
    }


    res = await getHardware('mothers')
    let mothers = [];
    if(res.ok){
        res.data.map((mother) => {
            let aux = {
                id: mother[0],
                name: mother[1],
                precio: mother[2],
                stock: mother[3],
                socket: mother[4],
                m2: mother[5],
                wats: mother[6]
            }
            mothers.push(aux)
            return 0;
        })
    }

    let cheaperPCwithGPU = getCheaperPCwithGPU(procesadores, gpus, rams, mothers)


    finalBudget = getFinalBudget(budgetFraction, cheaperPCwithGPU.price <= budgetFraction, _kind, procesadores, gpus, rams, mothers);
    finalBudget.rest = _budget - finalBudget.price;
    return {
        ok: true,
        data: finalBudget
    }
    
}

export default getBudget;
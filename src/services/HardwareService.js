const URL = "http://localhost:5050";

export const getHardware = async (tableName) => {
    try{
        const response = await fetch(`${URL}/hardware/${tableName}`);
        if(response){
            const resData = await response.json();
            console.log(resData)
            return {
                ok: true,
                data: resData
            };
        }
    }
    catch(e){
        console.error(e);
        return {ok: false}
    }
}


export const getAMD = async () => {
    try{
        const response = await fetch(`${URL}/AMD`);
        if(response){
            const resData = await response.json();
            return {
                ok: true,
                data: resData
            };
        }
    }
    catch(e){
        console.error(e);
        return {ok: false}
    }
}
export const getIntel = async () => {
    try{
        const response = await fetch(`${URL}/Intel`);
        if(response){
            const resData = await response.json();
            return {
                ok: true,
                data: resData
            };
        }
    }
    catch(e){
        console.error(e);
        return {ok: false}
    }
}
export const getRX = async () => {
    try{
        const response = await fetch(`${URL}/RX`);
        if(response){
            const resData = await response.json();
            return {
                ok: true,
                data: resData
            };
        }
    }
    catch(e){
        console.error(e);
        return {ok: false}
    }
}
export const getNvidia = async () => {
    try{
        const response = await fetch(`${URL}/Nvidia`);
        if(response){
            const resData = await response.json();
            return {
                ok: true,
                data: resData
            };
        }
    }
    catch(e){
        console.error(e);
        return {ok: false}
    }
}

import React,{useState, useEffect, useCallback} from 'react';
import LoadingBudget from '../../../components/loaders/LoadingBudget'
import getBudget from '../../../services/BudgetService'
import ResultCell from './ResultCell'
import CpuIcon from '../../../assets/images/2x/cpu.png'
import GpuIcon from '../../../assets/images/2x/gpu.png'
import RamIcon from '../../../assets/images/2x/ram.png'
import MotherIcon from '../../../assets/images/2x/mother.png'
import DollarIcon from '../../../assets/images/2x/money.png'
import './resultados.css'

const ShowBudget = (props) => {
    const [loading, setLoading] = useState(true);
    const [budget, setBudget] = useState({ok: false});

    const getBudgetFunc = useCallback( async () =>{
        try{
            const res = await getBudget(props.budget, props.kind, props.settings);
            if(res.ok){
                setBudget(res);
            }
        }catch(e){
            console.error(e);
            setLoading(false);
        }
    }, [props, budget]);

    useEffect(() => {
        getBudgetFunc();
    }, [])

    useEffect(() =>{
        console.log("budget: ", budget);
        if(budget.ok) setLoading(false);
    }, [budget])

    return (
        <div className={loading ? "loadingContainer" : "showBudgetContainer"}>
            {loading ?  <LoadingBudget /> :
            
                <div className="results">
                    <ResultCell icon={CpuIcon} data={budget.data.cpu} price={false}/>
                    <ResultCell icon={GpuIcon} data={budget.data.gpu} price={false}/>
                    <ResultCell icon={RamIcon} data={budget.data.ram} price={false}/>
                    <ResultCell icon={MotherIcon} data={budget.data.mother} price={false}/>
                    <ResultCell icon={DollarIcon} data={budget.data.price} price={true}/>
                </div>
            }
        </div>
    )
}

export default ShowBudget

import React from 'react'

const ResultCell = (props) => {
    console.log(props)
    return (
        <div className="resultCell">
            <img src={props.icon}></img>
            <div className="resultCellInfo">
                {
                    props.price ? <h2>{props.data}</h2> : <h4>{props.data.name}</h4>
                }
            </div> 
        </div>
    )
}

export default ResultCell

import React from 'react';
import CustomButton from '../../components/UI/Inputs/CustomButton';


const Settings = (props) => {

    return (
        <div className="settingsContainer">
            <h1>Preferencias</h1>
            <h3>A continuación puede especificar sus preferencias respecto a algunos aspectos técnicos de su computadora, si no tiene conocimiento sobre hardware o no desea setear
                 preferencias, puede simplemente clickear en continuar y el sistema se encargara de armar la mejor computadora para usted.</h3>
            <div className="buttonsContainer">
                <CustomButton text="SETTINGS" click={() =>  props.setModal(true)}/>
                <CustomButton text="TERMINAR" click={() => props.navigation('NEXT')}/>
            </div>
        </div>
    )
}

export default Settings

import React from 'react';
import TextInput from '../../components/UI/Inputs/TextInput';
import NextButton from '../../components/UI/Inputs/NextButton';

const MoneyInput = (props) => {
    return (
        <div className="MoneyInputDiv">
            <h1>¿Cuál es su presupuesto máximo para armar la PC?</h1>
            <TextInput label="Presupuesto" color="rgb(255, 111, 0)" type={"number"} setBudget={props.setBudget}/>
            <NextButton text="SIGUIENTE" navigation={props.navigation}/>
        </div>
    )
}

export default MoneyInput

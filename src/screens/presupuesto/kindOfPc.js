import React from 'react';
import BigButton from './../../components/UI/Inputs/BigButton'

const KindOfPc = (props) => {
    return (
        <div className="KindInputDiv">
            <BigButton text="Trabajo" hoverText="¡A trabajar!" selected={(props.kind === 'Trabajo')} setSelected={props.setKind}/>
            <BigButton text="Gaming" hoverText="¡A jugar!" selected={(props.kind === 'Gaming')} setSelected={props.setKind}/>
        </div>
    )
}

export default KindOfPc

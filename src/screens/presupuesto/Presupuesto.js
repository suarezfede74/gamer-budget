import React, { useState } from "react";
import MoneyInput from "./MoneyInput";
import KindOfPc from "./KindOfPc";
import Settings from "./Settings";
import BackButton from "../../components/UI/Inputs/BackButton";
import ErrorModal from "../../components/modals/ErrorModal";
import SettingsModal from '../../components/modals/SettingsModal';
import ShowBudget from './resultados/ShowBudget';
import "./presupuesto.css";

const Presupuesto = () => {
  const [budget, setBudget] = useState(-1);
  const [kind, setKind] = useState("");
  const [stepCount, setStepCount] = useState(1);
  const [settings, setSettings] = useState({
    setted: false,
    pref: {
      cpu: '',
      gpu: '',
    }
  });
  const [settingsModal, setSettingsModal] = useState(false);
  const [error, setError] = useState({
    show: false,
    title: "",
    text: "",
  });

  const handleBudgetNavigation = (op) => {
      if (op === "NEXT") {
        if (budget < 25000) {
          setError({
            show: true,
            title: "EL VALOR INGRESADO NO ES VÁLIDO",
            text: "Para poder continuar es necesario que ingrese un monto positivo y mayor a $ARS 25 000.",
          });
        }
        else{
            setStepCount(stepCount + 1);
        }
      } else if(op === "BACK"){
        switch(stepCount){
            case 2: setBudget(-1); break;
            case 3: setKind(""); break;
            default: break;
        }
        setStepCount(stepCount - 1);
        
      }
    }

  const kindSelectHandler = (value) => {
    setKind(value);
    handleBudgetNavigation("NEXT");
  }

  return (
    <div className="presupuesto">
      {stepCount > 1 && stepCount < 4? (
        <BackButton navigation={handleBudgetNavigation} setError={setError} />
      ) : null}
      {stepCount <= 1 ? (
        <MoneyInput
          budget={budget}
          setBudget={setBudget}
          navigation={handleBudgetNavigation}
          setError={setError}
        />
      ) : null}
      {stepCount === 2 ? (
        <KindOfPc
          kind={kind}
          setKind={kindSelectHandler}
        />
      ) : null}
      {stepCount === 3 ? (
        <Settings
          budget={budget}
          navigation={handleBudgetNavigation}
          setError={setError}
          setModal={setSettingsModal}
        />
      ) : null}
      {stepCount === 4 ? (
        <ShowBudget
          setError={setError}
          budget={budget}
          kind={kind}
          settings={settings}
        />
      ) : null}
      <ErrorModal error={error} setError={setError}/>
      <SettingsModal show={settingsModal} setShow={setSettingsModal} setSettings={setSettings}/>
    </div>
  );
};

export default Presupuesto;

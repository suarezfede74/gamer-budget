import React from 'react';
import BackButton from '../../components/UI/Inputs/BackButton'
import NextInstructions from '../../components/UI/Inputs/NextInstructions'

const InstructionContainer = (props) => {
    return (
        <div className="instructionContainer">
            <p className="instructionTitle">{props.title}</p>
            <p className="instructionDescription">{props.description}</p>
            <div className="instructionArrowContainer">
                {props.showArrows[0] && <BackButton navigation={props.navigation}/>}
                {props.showArrows[1] && <NextInstructions navigation={props.navigation}/>}
            </div>
        </div>
    )
}

export default InstructionContainer
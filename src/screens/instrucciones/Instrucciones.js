import React, {useState, useCallback} from 'react';
import './instrucciones.css'
import InstructionContainer from './InstructionContainer'

const Instrucciones = (props) => {
    const [stepCount, setStepCount] = useState(0);
    const steps = [
        { 
            title: 'Paso 1: La fuente de alimentación',
            description: 'En principio, si hemos adquirido un gabinete que no incluía fuente de alimentación, lo primero será montar la fuente al chasis del gabinete, para lo cual se la ubica de manera que el ventilador disipador de la misma quede orientado hacia la parte posterior de la carcasa, y que los cables de alimentación queden hacia el interior. Una vez colocada, debe ser sujetada con los tornillos.'
        },
        { 
            title: 'Paso 2.1: Encontrar el zócalo',
            description: 'Lo primero que haremos es identificar el zócalo en el que se ubicará el procesador el cual es fácilmente detectable, ya que por lo general es grande y posee varios agujeros circulares, además de tener la indicación de las siglas ZIF. La ubicación correcta del procesador estará definida de acuerdo con la alineación de la ranura. El CPU posee una marca o una de sus esquinas recortada que es lo que nos permitirá colocarlo de manera correcta, ya que obviamente esta marca debe coincidir exactamente con la que tiene el zócalo.'
        },
        { 
            title: 'Paso 2.2: Colocar el procesador',
            description: 'Colocamos con cuidado el procesador sobre el zócalo, y sin ejercer mucha presión insertamos los pines del procesador en las ranuras del zócalo. Para ello no debemos hacer fuerza, sólo dejar que el CPU se integre al zócalo. En el caso de que esto no suceda, posiblemente se deba a que el procesador posee alguno de sus pines doblados, el cual podremos enderezar con mucho cuidado con la ayuda de un destornillador plano. Luego bajamos la sujeción del zócalo, a fin de que el procesador quede bien fijado a la motherboard.'
        },
        { 
            title: 'Paso 2.3: Refrigeración',
            description: 'Una vez realizada la tarea debemos montar un disipador sobre el CPU utilizando los anclajes que incluye el zócalo. Es probable que antes de colocar el disipador nos veamos en la necesidad de colocar pasta térmica, de forma cuidadosa aplicar una gota en el centro, siempre evitando que se acumule demasiada pasta, y luego recién podremos colocar el disipador. Siempre es recomendable adquirir un procesador que incluya un disipador de fábrica, a fin de evitar errores de principiantes.'
        },
        { 
            title: 'Paso 3: La motherboard a la caja',
            description: 'Lo siguiente será montar la motherboard al chasis del gabinete, para lo cual sólo debemos alinear la motherboard de forma correcta a la bandeja que incluye el chasis para ello, y luego fijarla con los tornillos que vienen de fábrica con la carcasa. Debemos asegurarnos que los distintos conectores para queden debidamente ubicados, con el fin de posteriormente poder montar las placas que creamos necesarias para nuestra computadora.'
        },
        { 
            title: 'Paso 4: Colocar memoria RAM',
            description: 'Los módulos de memoria RAM pueden colocarse antes o después de colocar la motherboard en el chasis del gabinete. Eso depende de la decisión de cada uno. Una vez que identificamos los bancos para la memoria RAM, debemos colocar cuidadosamente cada módulo sobre ellos y empujar el mismo lentamente y sin ejercer fuerza hasta que escuchemos un clic. Ese sonido nos permitirá saber que la memoria RAM se ha instalado de forma correcta.'
        },
        { 
            title: 'Paso 5: Conexión de cables',
            description: 'Antes de continuar introduciendo componentes, es recomendable conectar algunos cables a la motherboard. En principio debemos conectar la motherboard a la fuente de alimentación. También es conveniente conectar los cables del panel frontal del gabinete, tanto el de Power y Reset como los leds, los puertos USB donde conectaremos los periféricos y demás. Para hacerlo lo mejor es recurrir al manual de la motherboard, donde se indica claramente cómo deben ser conectados de acuerdo al modelo.'
        },
        { 
            title: 'Paso 6: Placas dedicadas',
            description: 'Si hemos decidido incluir en la PC alguna placa extra, como puede ser una placa de video, una de Wi-Fi o una de Ethernet, este es el momento preciso para montarla. Para ello debemos identificar el zócalo PCI-E PCI o AGP en el que debe ir montada la placa. Se la coloca de forma que los pines coincidan con el conector y presionamos sin fuerza hasta escuchar el clic que nos indica que ha sido fijada.'
        },
        { 
            title: 'Paso 7.1: Colocar dispositivos',
            description: 'Lo siguiente es colocar el resto de dispositivos que hayamos elegido para nuestra PC, entre ellos podemos encontrar dispositivos de almacenamiento de estado sólido SATA, dispositivos de almacenamiento de estado sólido M.2, discos rígidos HDD, coolers para el gabinete, entre otros.'
        },
        { 
            title: 'Paso 7.2: SSD SATA',
            description: 'Estos son los dispositivos de almacenamiento de estado sólido (Solid State Drive, SSD). Este tipo de disposipositivo de almacenamiento es mucho más rápido que los discos rígidos convencionales, incrementando su velocidad alrededor de unas 60 veces en sus versiones más lentas. Estos dispositivos necesitaran una conección a la fuente de alimentación y otra a los puertos SATA de la placa madre, suelen atornillarse al gabinete.'
        },
        { 
            title: 'Paso 7.3: HDD SATA',
            description: 'Estos dispositivos son mas lentos y anticuados, pero tambien son mucho más baratos (se consiguen por el mismo precio un SSD de 120gb que un HDD de 1024gb). Estos dispositivos se conectan de la misma forma que los anteriormente mencionados. Cabe resaltar que son ligeramente ruidosos, en especial en comparación con un SSD.'
        },
        { 
            title: 'Paso 7.4: SSD M.2 y NVMe',
            description: 'Estos dispositivos de almacenamiento son unos de los mejores que podemos llegar a conseguir, se conectan directamente a la placa madre. Su puerto puede ser ligeramente difícil de encontrar, podrias googlear "M.2 motherboard port" e ir a la seccion de imágenes para ver como luce.'
        },
        { 
            title: 'Paso 8: Orden ante todo',
            description: 'Ya casi hemos concluido con el armado. Pero antes de cerrar el gabinete es recomendable acomodar los cables que hayan quedado sueltos sin ser utilizados, y que los ordenemos dentro del gabinete utilizando precintos o bandas elásticas. Le echamos un último vistazo y una última revisión para corroborar que todos los cables y conexiones se encuentren correctamente ajustados, y ya podemos poner las tapas del gabinete.'
        },
        { 
            title: 'Paso 9: El momento decisivo',
            description: 'Sólo nos resta ahora conectar al CPU los periféricos, es decir el monitor, el teclado y el mouse, como así también el cable para la energía eléctrica. Si todo sale bien, y el equipo enciende, sólo restará configurar los parámetros de la computadora y comenzar a instalar el sistema operativo que hayamos elegido.'
        },
    ]
    const totalSteps = steps.length;

    const navigation = useCallback((direction) =>{
        if(direction === 'BACK') setStepCount(stepCount - 1)
        else setStepCount(stepCount + 1)
    }, [stepCount])

    return (
        <div className="instrucciones">
            <InstructionContainer totalSteps={totalSteps} navigation={navigation} showArrows={[stepCount > 0, stepCount < totalSteps - 1]} title={steps[stepCount].title} description={steps[stepCount].description}/>
        </div>
    )
}

export default Instrucciones

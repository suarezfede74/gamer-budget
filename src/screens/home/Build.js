import React from 'react';
import graphicCards from '../../assets/images/radeon-cards.png';

const Build = (props) => {
    return (
        <div className="homeBuild" >
            <h1>Aprende a ensamblar tu PC</h1>
            <h3 className="goToBtn"  onClick={() => props.changeNavigation("Instrucciones")}>Aprender cómo</h3>
            <img className="homeGrphicsCardImgs" src={graphicCards} alt="img/home"></img>
        </div>
    )
}

export default Build

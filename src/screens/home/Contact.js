import React from 'react'

const Contact = (props) => {
    return (
        <div className="homeContact" >
            <div className="homeContactFilter">
                <h1>Contactanos</h1>
                <h2>Responderemos lo antes posible</h2>
                <h3 className="goToBtn"  onClick={() => props.changeNavigation("Contacto")}>Contacto</h3>
            </div>
        </div>
    )
}

export default Contact

import React from 'react';
import Presentation from './Presentation';
import Build from './Build';
import Hire from './Hire';
import Us from './Us';
import Contact from './Contact';
import './home.css';

const Home = (props) => {
    return (
        <div className="home">
            <Presentation resolution={props.resolution} changeNavigation={props.changeNavigation}/>
            <Build resolution={props.resolution} changeNavigation={props.changeNavigation}/>
            <Hire resolution={props.resolution} changeNavigation={props.changeNavigation}/>
            <Us resolution={props.resolution} changeNavigation={props.change}/>
            <Contact resolution={props.resolution} changeNavigation={props.change} />
        </div>
    )
}

export default Home

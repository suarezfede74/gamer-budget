import React from 'react'

const Presentation = (props) => {
    return (
        <div className="presentation">
            <div className="topPresentationText">
                <h1>Conseguí tu PC ideal</h1>
                <h2>Al mejor precio del mercado</h2>
                <h3 onClick={() => props.changeNavigation("Presupuesto")} className="goToBtn">¡Vamos!</h3>
            </div>
        </div>
    )
}

export default Presentation;
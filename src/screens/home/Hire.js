import React from 'react';

const Hire = (props) => {
    return (
        <div className="homeHire" >
            <div className="homeHireFilter">
                <h1>Contrata un técnico</h1>
                <h3 className="goToBtn"  onClick={() => props.changeNavigation("Técnicos")}>Contratar</h3>
            </div>
        </div>
    )
}

export default Hire

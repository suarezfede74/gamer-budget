import React from 'react';

const Us = (props) => {
    return (
        <div className="homeUs" >
            <div className="homeUsFilter">
                <h1>Conoce más sobre nosotros</h1>
                <h3 className="goToBtn"  onClick={() => props.changeNavigation("Nosotros")}>Conocer más</h3>
            </div>
        </div>
    )
}

export default Us

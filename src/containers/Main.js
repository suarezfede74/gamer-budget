import React, { Component } from 'react';
import Header from '../components/UI/Header/Header';
import SwitchNavigation from '../Utils/SwitchNavigation'

export class Main extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
            navigation: "Home",
            resolution: window.screen.width,
        };
      }
      UNSAFE_componentWillMount = () => {
          if(window.location.pathname === "/") this.setNavigation("Home");
          else if(window.location.pathname !== "/Home"){
              let url = window.location.pathname;
              this.setNavigation(url.slice(1,url.length))
          }
      }

      setNavigation = (url) => {
          this.setState({navigation: url})
          try{
            window.history.pushState('', 'Title', '/'+url);
          }catch(e){
              console.error(url);
          }
      }

      setResolution = (r) =>{
          this.setState({resolution: r});
      }
    render() {
        return (
            <div className="main">
                <Header selected={this.state.navigation} changeNavigation={this.setNavigation} setResolution={this.setResolution}/>
                <SwitchNavigation navigation={this.state.navigation} resolution={this.state.resolution} changeNavigation={this.setNavigation}/>
            </div>
        )
    }
}

export default Main

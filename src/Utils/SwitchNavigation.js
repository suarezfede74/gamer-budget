import React from 'react';
import Home from '../screens/home/Home'
import Presupuesto from '../screens/presupuesto/Presupuesto'
import Instrucciones from '../screens/instrucciones/Instrucciones'
import Tecnicos from '../screens/tecnicos/Tecnicos'
import Nosotros from '../screens/nosotros/Nosotros'
import Contacto from '../screens/contacto/Contacto'

const SwitchNavigation = (props) => {
    switch(props.navigation){
        case 'Home': return <Home resolution={props.resolution} changeNavigation={props.changeNavigation}/>
        case 'Presupuesto': return <Presupuesto resolution={props.resolution}/>
        case 'Instrucciones': return <Instrucciones resolution={props.resolution}/>
        case 'Técnicos': return <Tecnicos resolution={props.resolution}/>
        case 'Nosotros': return <Nosotros resolution={props.resolution}/>
        case 'Contacto': return <Contacto resolution={props.resolution}/>
        default: return <Home resolution={props.resolution} changeNavigation={props.changeNavigation}/>
    }
}

export default SwitchNavigation

import React from "react";

const styles = {
  modalBack: {
    position: "absolute",
    width: "100vw",
    height: "100vh",
    backgroundColor: "rgba(30, 30, 30, .5)",
    zIndex: 1000,
    top: "-11vh",
    left: 0,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  modalContent: {
    color: "white",
    backgroundColor: "rgba(0, 0, 0, .9)",
    padding: "calc(1vw + 1vh)",
    borderLeft: "1px solid rgb(255, 111, 0)",
    maxWidth: "80vw",
  },
  h1: {
    fontSize: "2em",
    fontWeight: 400,
  },
  h3: {
    fontWeight: 200,
  },
  okButton: {
    float: "right",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: "5vw",
    border: "1px solid rgb(255, 111, 0)",
    borderRadius: "0",
    marginTop: "3vh",
    padding: ".3vw",
    cursor: "pointer",
    color: "rgb(255, 151, 0)",
    minWidth: "50px"
  },
};

const ErrorModal = (props) => {
  const cleanError = () => {
    props.setError({
      show: false,
      title: "",
      text: "",
    });
  };

  if (props.error.show) {
    return (
      <div style={styles.modalBack}>
        <div style={styles.modalContent}>
          <h1 style={styles.h1}>{props.error.title}</h1>
          <br />
          <h3 style={styles.h3}>{props.error.text}</h3>
          <div style={styles.okButton} onClick={cleanError}>
            OK
          </div>
        </div>
      </div>
    );
  } else {
    return <div></div>;
  }
};

export default ErrorModal;

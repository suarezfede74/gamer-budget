import React, { useState } from "react";

const styles = {
  modalBack: {
    position: "absolute",
    width: "100vw",
    height: "100vh",
    backgroundColor: "rgba(30, 30, 30, .5)",
    zIndex: 1000,
    top: "-11vh",
    left: 0,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  modalContent: {
    color: "white",
    backgroundColor: "rgba(0, 0, 0, .9)",
    padding: "calc(1vw + 1vh)",
    borderLeft: "1px solid rgb(255, 111, 0)",
    maxWidth: "80vw",
    minWidth: "40vw",
    maxHeight: "70vh",
    minHeight: "30vh",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-evenly",
  },
  h1: {
    fontSize: "2em",
    fontWeight: 400,
  },
  select: {
    display: "flex",
    flexDirection: "row",
    width: "50%",
    justifyContent: "space-evenly",
  },
  selectCPU: {
    fontSize: "1.5em",
    fontWeight: "400",
  },
  option: {
    width: "5.5vw",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  buttonsContainer: {
    width: "80%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  button: {
    float: "right",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: "5vw",
    border: "1px solid rgb(255, 111, 0)",
    borderRadius: "0",
    marginTop: "3vh",
    padding: ".3vw",
    cursor: "pointer",
    color: "rgb(255, 151, 0)",
    minWidth: "50px",
  },
};

const SettingsModal = (props) => {
  const [cpu, setCpu] = useState("");
  const [gpu, setGpu] = useState("");

  const okClickHandler = () => {
      props.setSettings({
        setted: true,
        pref: {
          cpu: cpu,
          gpu: gpu,
        }
      })
      props.setShow(false)
  }
  const cancelClickHandler = () => {
      setCpu('');
      setGpu('');
    props.setShow(false)
}

  if (props.show) {
    return (
      <div style={styles.modalBack}>
        <div style={styles.modalContent}>
          <h1 style={styles.h1}>Preferencias</h1>
          <div style={styles.select}>
            <div style={styles.selectCPU}>
              <div style={styles.option}>
                AMD{" "}
                <input
                  type="radio"
                  onClick={() => setCpu("AMD")}
                  checked={cpu === "AMD"}
                />
              </div>
              <div style={styles.option}>
                Intel{" "}
                <input
                  type="radio"
                  onClick={() => setCpu("Intel")}
                  checked={cpu === "Intel"}
                />
              </div>
            </div>
            <div style={styles.selectCPU}>
              <div style={styles.option}>
                RX{" "}
                <input
                  type="radio"
                  onClick={() => setGpu("RX")}
                  checked={gpu === "RX"}
                />
              </div>
              <div style={styles.option}>
                NVIDIA{" "}
                <input
                  type="radio"
                  onClick={() => setGpu("NVIDIA")}
                  checked={gpu === "NVIDIA"}
                />
              </div>
            </div>
          </div>
          <div style={styles.buttonsContainer}>
            <div style={styles.button} onClick={cancelClickHandler}>
                CANCELAR
            </div>
            <div style={styles.button} onClick={okClickHandler}>
                OK
            </div>
          </div>
        </div>
      </div>
    );
  } else {
    return <div></div>;
  }
};

export default SettingsModal;

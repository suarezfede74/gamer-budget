import React from "react";
import "./loaders.css";

const LoadingBudget = () => {
  return (
      <div class="loaderContainer">
        <div class="loading">
          <p>Loading</p>
          <span></span>
        </div>
      </div>
  );
};

export default LoadingBudget;

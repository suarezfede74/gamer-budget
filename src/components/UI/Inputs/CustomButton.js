import React from 'react';

const CustomButton = (props) => {
    return (
        <div className="customButton" onClick={props.click}>
            {props.text}
        </div>
    )
}

export default CustomButton

import React from 'react';
import './inputs.css'

const BackButton = (props) => {
    return (
        <div className="backButton" onClick={() => props.navigation("BACK")}>
            <i className="fas fa-arrow-left"></i>
        </div>
    )
}

export default BackButton

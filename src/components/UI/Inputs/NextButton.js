import React from 'react';

const NextButton = (props) => {
    return (
        <div className="nextBtn from-left" onClick={() => props.navigation("NEXT")}>{props.text}</div>
    )
}

export default NextButton

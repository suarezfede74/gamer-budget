import React from 'react';

const TextInput = (props) => {
    return (
        <div className="form_group field">
            <input onChange={(e) => props.setBudget(e.target.value)} autoComplete="off" type={props.type} className="form_field" placeholder="Presupuesto" name="name" id='name' required />
            <label htmlFor="name" className="form_label">{props.label}</label>
        </div>    
    )
}

export default TextInput

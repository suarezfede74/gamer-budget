import React from 'react';

const NextInstructions = (props) => {
    return (
        <div className="nextInstruction" onClick={() => props.navigation("NEXT")}>
            <i className="fas fa-arrow-right"></i>
        </div>
    )
}

export default NextInstructions

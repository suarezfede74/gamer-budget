import React from 'react'

const BigButton = (props) => {
    return(
        <button data-hover={props.hoverText} className={props.selected ? 'bigButton bigButtonSelected' : 'bigButton'} onClick={() => props.setSelected(props.text)}><div>{props.text}</div></button>
    )
}

export default BigButton

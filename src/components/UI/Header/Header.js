import React, {useState, useEffect} from "react";
import "./header.css";
import hamburger from "../../../assets/menu.svg";
import logoMovile from "../../../assets/icon.png";
import logoDesktop from "../../../assets/logo_blanco.png";

const Header = (props) => {
    const [showMenu, setShowMenu] = useState(false);
    const [resolution, setResolution] = useState(0);

    const [loop, setLoop] = useState(true);
    useEffect(() => {
        if(window.screen.width !== resolution) setResolution(window.screen.width);
        setTimeout(() => {
            setLoop(!loop)
        }, 1000)
    }, [loop, resolution])

    if(resolution <= 1024){
        return (
          <div className="header">
            <div className="headerLogoMovile" >
              <img src={logoMovile} alt=""/>
            </div>
            <div className="hamburger" onClick={() => setShowMenu(true)}>
              <img src={hamburger} alt=""/>
            </div>
            <div className={showMenu ? "showMenu" : "hideMenu"}>
              <div className="transparentPart" onClick={() => setShowMenu(false)}></div>
              <div className="solidPart">
                  <div className="closeMenu" onClick={() => setShowMenu(false)}>X</div>
                  <div className="headerMenu">
                      <span className="menuTittle">MENÚ</span>
                      <span onClick={() =>{
                        props.changeNavigation("Home")
                        setShowMenu(false)
                        }} className={(props.selected === "Home") ? "menuItem selected" : "menuItem"}>Home</span>
                      <span onClick={() =>{
                        props.changeNavigation("Presupuesto")
                        setShowMenu(false)
                        }}  className={(props.selected === "Presupuesto") ? "menuItem selected" : "menuItem"} >Presupuesto</span>
                      <span onClick={() =>{
                        props.changeNavigation("Instrucciones")
                        setShowMenu(false)
                        }}  className={(props.selected === "Instrucciones") ? "menuItem selected" : "menuItem"} >Instrucciones</span>
                      <span onClick={() =>{
                        props.changeNavigation("Técnicos")
                        setShowMenu(false)
                        }}  className={(props.selected === "Técnicos") ? "menuItem selected" : "menuItem"} >Técnicos</span>
                      <span onClick={() =>{
                        props.changeNavigation("Nosotros")
                        setShowMenu(false)
                        }}  className={(props.selected === "Nosotros") ? "menuItem selected" : "menuItem"} >Nosotros</span>
                      <span onClick={() =>{
                        props.changeNavigation("Contacto")
                        setShowMenu(false)
                        }}  className={(props.selected === "Contacto") ? "menuItem selected" : "menuItem"} >Contacto</span>
                  </div>
              </div>
            </div>
          </div>
        );
    }
    else{
        return(
            <div className="header">
                <div className="headerLogoMovile" >
                    <img src={logoDesktop} alt=""/>
                </div>
                <div className="desktopMenu">
                    <span onClick={(e) => props.changeNavigation("Home")} className={(props.selected === "Home") ? "menuItem selected" : "menuItem"}>Home</span>
                    <span onClick={(e) => props.changeNavigation("Presupuesto")} className={(props.selected === "Presupuesto") ? "menuItem selected" : "menuItem"} >Presupuesto</span>
                    <span onClick={(e) => props.changeNavigation("Instrucciones")} className={(props.selected === "Instrucciones") ? "menuItem selected" : "menuItem"} >Instrucciones</span>
                    <span onClick={(e) => props.changeNavigation("Técnicos")} className={(props.selected === "Técnicos") ? "menuItem selected" : "menuItem"} >Técnicos</span>
                    <span onClick={(e) => props.changeNavigation("Nosotros")} className={(props.selected === "Nosotros") ? "menuItem selected" : "menuItem"} >Nosotros</span>
                    <span onClick={(e) => props.changeNavigation("Contacto")} className={(props.selected === "Contacto") ? "menuItem selected" : "menuItem"} >Contacto</span>
                </div>
            </div>
        );
    }
};

export default Header;
